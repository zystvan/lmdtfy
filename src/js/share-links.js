var shareLinks = $A('.share-link'),
    fullLink = $('#full-link'),
    shortLink = $('#short-link'),
    oldValue,
    oldShort;

var shortenLink = function(link) {
  oldValue = searchBox.value;

  fetch(link.href)
    .then(function(response) {
      return response.json();
    }).then(function(json) {
      link.href = json["shorturl"];
      oldShort = link.href;
    });
}

var copyLink = function(link) {
  var linkToCopy = link;

  if (this instanceof HTMLElement && !(link instanceof HTMLElement)) {
    linkToCopy = this;
  } else if (!(this instanceof HTMLElement) && !(link instanceof HTMLElement)) {
    console.error("Failed to copy link - invalid element");

    return false;
  }

  var originalText = linkToCopy.textContent;
  linkToCopy.textContent = "Copying";

  var input = document.createElement("input");
  input.value = linkToCopy.href;
  document.body.appendChild(input);

  input.select();

  try {
    var copied = document.execCommand("copy");

    // get rid of the input so the user never even sees it
    document.body.removeChild(input);

    // just in case
    if (copied === false) { throw "Failed to copy"; }
  } catch(e) {
    prompt("Copy to clipboard: Ctrl/Cmd + C, Enter", linkToCopy.href);
  }

  setTimeout(function() {
    linkToCopy.textContent = "Copied!";

    setTimeout(function() {
      linkToCopy.textContent = originalText;
    }, 1000);
  }, 400);
}

searchBox.addEventListener("keyup", function() {
  fullLink.href = "/?q=" + encodeURIComponent(this.value);

  if (this.value === oldValue) {
    shortLink.href = oldShort;
  } else {
    shortLink.href = "https://is.gd/create.php?format=json&url=" + encodeURIComponent(location + "?q=" + this.value);
  }
});

shortLink.addEventListener("click", function(e) {
  if (searchBox.value !== oldValue) {
    shortenLink(this);

    this.textContent = "Click to copy";
  } else {
    copyLink(this);
  }
});

fullLink.addEventListener("click", copyLink);

shareLinks.forEach(function(link) {
  link.addEventListener("click", function(e) {
    e.preventDefault();
    return false;
  });
});