if (localStorage.theme !== undefined) {
  document.body.classList.remove("light-theme");
  document.body.classList.add(localStorage.theme + "-theme");
}

$('#light-toggle').addEventListener("click", function() {
  document.body.classList.remove("dark-theme");
  document.body.classList.add("light-theme");
  localStorage.setItem("theme", "light");
});

$('#dark-toggle').addEventListener("click", function() {
  document.body.classList.remove("light-theme");
  document.body.classList.add("dark-theme");
  localStorage.setItem("theme", "dark");
});
