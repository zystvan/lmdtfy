var typer = function(input, text, callback) {
  var i = 0;

  var typing = setInterval(function() {
    if (i >= text.length - 1) {
      if (callback) { callback(); }

      clearInterval(typing);
    }

    input.value += text[i];

    i += 1;
  }, 100);
}