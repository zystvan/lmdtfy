var animate = function(element, x, y, duration, callback) {
  var startPos = element.getBoundingClientRect(),
      deltaX = x - startPos.x,
      deltaY = y - startPos.y,
      stepX = deltaX / (100 * duration),
      stepY = deltaY / (100 * duration),
      counter = 0;

  var animation = window.setInterval(function() {
    if (counter == (duration * 1000) / 10) {
      if (callback) { callback(); }

      window.clearInterval(animation);
    }

    currentPos = element.getBoundingClientRect();

    element.style.left = (currentPos.x + stepX) + "px";
    element.style.top = (currentPos.y + stepY) + "px";

    counter++;
  }, 10);
}