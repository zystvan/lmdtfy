var searchContainer = $('.search-container'),
    searchForm = $('.search-form'),
    searchBox = $('#search-box'),
    searchBoxPos = searchBox.getBoundingClientRect(),
    searchSubmit = $('#search-submit'),
    searchSubmitPos = searchSubmit.getBoundingClientRect(),
    cursor = $('#cursor'),
    cursorPos = cursor.getBoundingClientRect(),
    search = location.search;

search = search.substr(3);
    
if (search.length > 0) {
  cursor.classList.remove("hidden");

  var cursorWatch = new MutationObserver(function(mutations) {
    mutations.forEach(function (mutation) {
      cursorPos = cursor.getBoundingClientRect();

      // break out of this if it's not the attribute we want
      // otherwise, we get an infinite loop because of modifying the classes
      if (mutation.attributeName !== "style") { return false }

      if (cursorPos.x >= searchSubmitPos.x &&
          cursorPos.y >= searchSubmitPos.y) {
        cursor.classList.remove("cursor-text");
        cursor.classList.add("cursor-pointer");

        if (!searchSubmit.classList.contains("hover")) {
          searchSubmit.classList.add("hover");
        }
      } else if (cursorPos.x >= searchBoxPos.x &&
                  cursorPos.y >= searchBoxPos.y) {
        cursor.classList.remove("cursor-default");
        cursor.classList.add("cursor-text");
      }
    });
  });
  
  cursorWatchConfig = { attributes: true };
  cursorWatch.observe(cursor, cursorWatchConfig);

  // `+ .5` is a fix for the animate() function not being quiiite accurate
  animate(cursor, searchBoxPos.x + .5, searchBoxPos.y + 15, 1, function() {
    cursorWatch.disconnect();

    setTimeout(function() {
      searchContainer.classList.add("active");

      setTimeout(function() {
        searchContainer.classList.remove("active");

        setTimeout(function() {
          typer(searchBox, search, function() {
            cursorWatch.observe(cursor, cursorWatchConfig);

            animate(cursor, searchSubmitPos.x + 10, searchSubmitPos.y + 5, 2, function() {
              cursorWatch.disconnect();

              setTimeout(function() {
                setTimeout(function() {
                  searchSubmit.classList.add("active");

                  // searchForm.submit();
                }, 250);
              }, 100);
            });
          });
        }, 200);
      }, 150);
    }, 100);
  });
}