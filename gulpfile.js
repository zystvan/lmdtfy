var gulp = require('gulp'),
    browserSync = require('browser-sync').create(),
    imagemin = require('gulp-imagemin'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    cleanCSS = require('gulp-clean-css'),
    autoprefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat');

gulp.task("default", function() {
  console.log("Gulp is working!");
});

gulp.task("reload", function() {
  return browserSync.reload({
    stream: true
  });
});

gulp.task("serve", ["images", "styles", "scripts"], function() {
  browserSync.init({
    proxy: "lmdtfy.dev"
  });

  gulp.watch("./src/img/*", ["images"]);
  gulp.watch("./src/css/*", ["styles"]);
  gulp.watch("./src/js/*", ["scripts"]);
  gulp.watch("./*.html", ["reload"]);
});

gulp.task("images", function() {
  return gulp
    .src("./src/img/*")
    .pipe(imagemin())
    .pipe(gulp.dest("./assets/img/"));
});

gulp.task("styles", function() {
  return gulp
    .src("./src/css/*")
    .pipe(sourcemaps.init())
    .pipe(sass({
      outputStyle: "compressed"
    })
    .on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(cleanCSS())
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest("./assets/css/"))
    .pipe(browserSync.reload({
      stream: true
    }));
});

gulp.task("scripts", function() {
  return gulp
    .src(["./src/js/base.js", "./src/js/theme.js", "./src/js/typer.js", "./src/js/animate.js", "./src/js/cursor.js", "./src/js/share-links.js"])
    .pipe(sourcemaps.init())
    .pipe(uglify({
      mangle: true
    }))
    .pipe(concat('main.js'))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest("./assets/js/"))
    .pipe(browserSync.reload({
      stream: true
    }));
});